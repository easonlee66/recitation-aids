# 背诵辅助工具

## 使用
可以在[Release](https://gitee.com/easonlee66/recitation-aids/releases)中查找已编译版本。

在没有Mac机器的情况下提供Mac版本是一件非常滑稽的事情，所以Mac用户请用make自行编译。

你也可以在公众号文章中查看使用教程。